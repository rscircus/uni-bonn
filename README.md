# uni-bonn

All kinds of projects in the context of Uni Bonn

## GeomLab 2020 - Visibility and stacking

The basic problem can be roughly understood as follows: Given a set S of points $`p_i = (x_i, y_i)`$, where $`p_i \in \mathbb{R}^2`$. Each point $`p_i`$ has a bijective relation $`r(p_i)`$ with some kind of value (from a dataset) which corresponds to the radius of a disc with $`p_i`$ being in the center.

Or in other words, we're looking at a bunch of discs (or even potentially other geometrical objects) probably covering each other.

Now the question is: *How can we guarantee that we still see the information in a reasonable way?* That is: Every disc is visble and 'bears full information', while being visible. A counterexample for the latter case might be a disc covered by other discs in such a way that just a piece of the disc's area is visible without any bit of the perimeter. In that case it would be impossible to estimate the radius nor center of that only slightly visible disc for the human brain.

### Discussion

The outline problem relates to several fields. Namely:

- Visibility - as in https://en.wikipedia.org/wiki/Visibility_(geometry)
- Sorting - as in https://en.wikipedia.org/wiki/Sorting_algorithm
